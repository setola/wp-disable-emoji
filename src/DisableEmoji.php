<?php
namespace Setola\WPDisableEmoji;

use Setola\Singleton\Singleton;

/**
 * Created by PhpStorm.
 * User: emanuele.tessore
 * Date: 29/02/2016
 * Time: 12:54
 * Credits: http://wordpress.stackexchange.com/questions/185577/disable-emojicons-introduced-with-wp-4-2
 */
class DisableEmoji extends Singleton {

	public function __construct() {
		add_action('init', array($this, 'disableWPEmojicons'));
		add_filter('tiny_mce_plugins', array($this, 'disableTinymceEmojicons'));
	}

	function disableWPEmojicons() {
		remove_action('admin_print_styles', 'print_emoji_styles');
		remove_action('wp_head', 'print_emoji_detection_script', 7);
		remove_action('admin_print_scripts', 'print_emoji_detection_script');
		remove_action('wp_print_styles', 'print_emoji_styles');
		remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
		remove_filter('the_content_feed', 'wp_staticize_emoji');
		remove_filter('comment_text_rss', 'wp_staticize_emoji');
	}

	function disableTinymceEmojicons($plugins) {
		if(is_array($plugins)) {
			return array_diff($plugins, array('wpemoji'));
		} else {
			return array();
		}
	}
}